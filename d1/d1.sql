--crud operation

-- inserting records to particular table and 1 column
INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("Psy");

-- inserting records to particular table and 2 or more column

INSERT INTO albums 
	(
	album_title,
	date_released,
	artist_id
  	) 
	VALUES 
		(
		 "Psy 6",
		 "2012-1-1",
		 2
		 );

INSERT INTO albums 
	(
	album_title,
	date_released,
	artist_id
  	) 
	VALUES 
		(
		 "Trip",
		 "1996-1-1",
		 1
		 );

INSERT INTO songs 
	(
	song_name,
	length,
	genre,
	album_id
	)
	VALUES
		(
		"Gangnam Style",
		253,
		"Kpop",
		4
		);

INSERT INTO songs 
	(
	song_name,
	length,
	genre,
	album_id
	)
	VALUES
		(
		"ulan",
		254,
		"OPM",
		5 
		);

INSERT INTO songs 
	(
	song_name,
	length,
	genre,
	album_id
	)
	VALUES
		(
		"214",
		904,
		"OPM",
		5
		);

--READ or SELECT 

--title and genre only 
SELECT song_name, genre FROM songs;

--display the song name of all the opm songs
SELECT song_name 
FROM songs 
WHERE genre = "OPM";

--display the title of all the songs
SELECT * FROM songs;


--display the title and length that are more than 2 minutes
SELECT song_name, length
FROM songs
WHERE length > 901 
AND genre = "OPM";


--updating records

UPDATE songs
SET length = 504
WHERE song_name = "214";

UPDATE songs
SET song_name = "ulan updated"
WHERE song_name = "ulan";

--delete a record
/*
	DELETE - row
	DROP - database
*/
DELETE FROM songs WHERE genre = "OPM" AND length > 255;



