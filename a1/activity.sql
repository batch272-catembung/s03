--insert data to table users
INSERT INTO users
(
    email,
    password,
    datetime_created
)
VALUES
    (
    "jonnsmith@gmail.com",
    "passwordA",
    '2022-01-01 01:00:00'
    ),

    (
    "juandelacruz@gmail.com",
    "passwordB",
    '2022-01-01 02:00:00'
    ),

    (
    "mariadelacruz@gmail.com",
    "passwordC",
    '2022-01-01 03:00:00'
    ),

    (
    "mariadelacruz@gmail.com",
    "passwordD",
    '2022-01-01 04:00:00'
    ),

    (
    "johndoe@gmail.com",
    "passwordE",
    '2022-01-01 05:00:00'
    )
;


--insert data to table post
INSERT INTO posts
(
    user_id,
    title,
    content,
    datetime_posted
)
VALUES
    (
    1,
    "First Code",
    "Hello World!",
    '2022-01-02 01:00:00'
    ),

    (
    1,
    "Second Code",
    "Hello Earth!",
    '2022-01-02 02:00:00'
    ),

    (
    2,
    "Third Code",
    "Welcome to mars!",
    '2022-01-02 03:00:00'
    ),

    (
    4,
    "Fourth Code",
    "Bye bye solar system!",
    '2022-01-02 04:00:00'
    )
;


--Get all the post with an Author ID of 1.
SELECT * 
FROM users 
WHERE id=1;

--Get all the user's email and datetime of creation.
SELECT email, datetime_created 
FROM users;

--Update a post's content to "Hello to the people of the Earth!" where its initial content is "Hello Earth!" by using the record's ID.
UPDATE posts
SET content="Hello to the people of the Earth!"
WHERE content="Hello Earth!";

--Delete the user with an email of "johndoe@gmail.com".
DELETE FROM users 
WHERE email ="johndoe@gmail.com" ;





-- CREATE DATABASE blog_db;
-- USE blog_db;

-- CREATE TABLE users (
--     id INT NOT NULL AUTO_INCREMENT,
--     email VARCHAR(100) NOT NULL,
--     password VARCHAR(50) NOT NULL,
--     datetime_created DATETIME NOT NULL,
--     PRIMARY KEY (id)
-- );

-- CREATE TABLE posts (
--     id INT NOT NULL AUTO_INCREMENT,
--     user_id INT NOT NULL,
--     title VARCHAR(200) NOT NULL,
--     content VARCHAR(5000) NOT NULL,
--     datetime_posted DATETIME NOT NULL,
--     PRIMARY KEY (id),
--     CONSTRAINT fk_posts_user_id
--         FOREIGN KEY (user_id) REFERENCES users(id)
--         ON UPDATE CASCADE
--         ON DELETE RESTRICT
-- );

-- CREATE TABLE post_likes (
--     id INT NOT NULL AUTO_INCREMENT,
--     user_id INT NOT NULL,
--     post_id INT NOT NULL,
--     datetime_liked DATETIME NOT NULL,
--     PRIMARY KEY (id),
--     CONSTRAINT fk_post_likes_user_id
--         FOREIGN KEY (user_id) REFERENCES users(id)
--         ON UPDATE CASCADE
--         ON DELETE RESTRICT,
--     CONSTRAINT fk_post_likes_post_id
--         FOREIGN KEY (post_id) REFERENCES posts(id)
--         ON UPDATE CASCADE
--         ON DELETE RESTRICT
-- );

-- CREATE TABLE post_comments (
--     id INT NOT NULL AUTO_INCREMENT,
--     user_id INT NOT NULL,
--     post_id INT NOT NULL,
--     content VARCHAR(5000) NOT NULL,
--     datetime_commented DATETIME NOT NULL,
--     PRIMARY KEY (id),
--     CONSTRAINT fk_post_comments_user_id
--         FOREIGN KEY (user_id) REFERENCES users(id)
--         ON UPDATE CASCADE
--         ON DELETE RESTRICT,
--     CONSTRAINT fk_post_comments_post_id
--         FOREIGN KEY (post_id) REFERENCES posts(id)
--         ON UPDATE CASCADE
--         ON DELETE RESTRICT
-- );


